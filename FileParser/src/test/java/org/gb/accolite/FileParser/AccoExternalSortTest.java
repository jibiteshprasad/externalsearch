package org.gb.accolite.FileParser;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

public class AccoExternalSortTest {

	@Test
	public void testExternalSort() {
		AccoExternalSort aes = new AccoExternalSort();
		File inputfile = new File("/home/neo/Workspace/accolite/FileParser/input/test.txt");
		File outputfile = new File("/home/neo/Workspace/accolite/FileParser/output/output.txt");
		try {
			List<File> tmpfilelist = aes.sortFile(inputfile, 3);
			aes.mergeSortFiles(tmpfilelist, outputfile);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}

}
