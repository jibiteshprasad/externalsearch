package org.gb.accolite.FileParser;

import java.util.Comparator;

/**
 * Comparator to be used in the PriorityQueue. I was getting a
 * NullPointerException here and hence the crazy checking of the values to make
 * sure they are not null.
 * 
 * @author neo
 * 
 */
public class TempFileComparator implements Comparator<TempFileBuffer> {

	public int compare(TempFileBuffer tfb1, TempFileBuffer tfb2) {
		if (tfb1 == null || tfb1.peek() == null)
			return -1;
		if (tfb2 == null || tfb2.peek() == null)
			return 1;
		return tfb1.peek().compareTo(tfb2.peek());
	}

}
