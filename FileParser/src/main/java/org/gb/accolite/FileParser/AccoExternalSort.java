package org.gb.accolite.FileParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The entry class for External Search. 
 * @author neo
 * 
 */
public class AccoExternalSort {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AccoExternalSort.class);

	/**
	 * The maximum number of temporary files which this code should generate.
	 * This number would vary as per use case.
	 * 
	 */
	private static final int DEFAULTMAXTMPFILES = 1024;

	/**
	 * Not the best way, but I guess this should do the trick.
	 * 
	 * @param filetobesorted
	 * @param maxtmpfiles
	 * @return
	 */
	private long defineBlockSize(File filetobesorted, int maxtmpfiles) {
		long filesize = filetobesorted.length() * 2;
		long blocksize = filesize / maxtmpfiles
				+ (filesize % maxtmpfiles == 0 ? 0 : 1);
		long freemem = Runtime.getRuntime().freeMemory();
		if (blocksize < freemem / 2) {
			blocksize = freemem / 2;
		}
		return blocksize;
	}

	/**
	 * Just a hook, in case the user is not certain of the maximum number of
	 * temporary files he should be using.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public List<File> sortFile(File file) throws IOException {
		return sortFile(file, DEFAULTMAXTMPFILES);
	}

	/**
	 * Breaking up the big file and sorting its smaller pieces is performed in
	 * this method.
	 * 
	 * @param file
	 * @param maxtmpfiles
	 * @return
	 * @throws IOException
	 */
	public List<File> sortFile(File file, int maxtmpfiles) throws IOException {

		List<File> tempfilelist = new ArrayList<File>();
		List<String> linelist = new ArrayList<String>();
		String line = "";
		int counter = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(file)));
		try {
			long blocksize = defineBlockSize(file, maxtmpfiles);
			// The block size can be used as 32 for testing purposes.
			// long blocksize=32;
			LOGGER.info("Block Size used: " + blocksize);
			do {
				long currentblocksize = 0;
				while ((currentblocksize < blocksize)
						&& ((line = br.readLine()) != null)) {
					linelist.add(line);
					currentblocksize += line.length() * 2;
				}
				LOGGER.debug("Number of lines in file: " + linelist.size());
				tempfilelist.add(sortLinesInTemp(linelist, counter));
				linelist.clear();
				counter++;
			} while (line != null);
		} catch (EOFException oef) {
			tempfilelist.add(sortLinesInTemp(linelist, counter++));
			linelist.clear();
		} finally {
			br.close();
		}
		LOGGER.info("Number of temp files created: " + tempfilelist.size());
		return tempfilelist;
	}

	/**
	 * Sorts the lines of string that is present in the smaller temporary file.
	 * 
	 * @param tmplist
	 * @param count
	 * @return
	 * @throws IOException
	 */
	public File sortLinesInTemp(List<String> tmplist, int count)
			throws IOException {
		LOGGER.debug("Sorting lines in a file.");
		Collections.sort(tmplist);
		File newtmpfile = File.createTempFile("exsearch" + count, ".tmp");
		newtmpfile.deleteOnExit();
		LOGGER.info("Writing the strings to a temp file: "
				+ newtmpfile.getAbsolutePath() + newtmpfile.getName());
		OutputStream out = new FileOutputStream(newtmpfile);
		BufferedWriter fbw = new BufferedWriter(new OutputStreamWriter(out));
		try {
			for (String r : tmplist) {
				fbw.write(r);
				fbw.newLine();
			}
		} finally {
			fbw.close();
		}
		return newtmpfile;
	}

	/**
	 * Merge the smaller files into the bigger output files.
	 * 
	 * @param tempfilelist
	 * @param outputfile
	 * @return
	 * @throws IOException
	 */
	public int mergeSortFiles(List<File> tempfilelist, File outputfile)
			throws IOException {

		LOGGER.debug("Initialising the Priority Queue with TempFileComparator and initial capacity: 3");
		PriorityQueue<TempFileBuffer> pq = new PriorityQueue<TempFileBuffer>(3,
				new TempFileComparator());
		for (File f : tempfilelist) {
			TempFileBuffer tfb = new TempFileBuffer(f);
			pq.add(tfb);
		}
		BufferedWriter fbw = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(outputfile, true)));
		int noofrows = 0;
		try {
			while (pq.size() > 0) {
				TempFileBuffer tfb = pq.poll();
				if (tfb.isEmpty()) {
					tfb.close();
					tfb.delete();
					continue;
				}
				String r = tfb.pop();
				fbw.write(r);
				fbw.newLine();
				noofrows++;
				if (tfb.isEmpty()) {
					tfb.close();
					tfb.delete();
				} else {
					pq.add(tfb);
				}
			}
		} finally {
			fbw.close();
		}
		LOGGER.debug("Number of rows written: " + noofrows);
		return noofrows;
	}

}
