package org.gb.accolite.FileParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper class to read the strings from the file without the loading the
 * complete file.
 * 
 * @author neo
 * 
 */
public class TempFileBuffer {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TempFileBuffer.class);

	private BufferedReader tbr;
	private String cache;
	private boolean isempty;
	private File filename;

	public TempFileBuffer(File tmpfile) {
		InputStream is = null;
		this.filename = tmpfile;
		isempty = false;
		try {
			is = new FileInputStream(tmpfile);
			this.tbr = new BufferedReader(new InputStreamReader(is));
			prefetchNextLine();
		} catch (FileNotFoundException e) {
			LOGGER.error("Could not find temporary file: " + tmpfile.getName(),
					e);
		}
	}

	public String peek() {
		if (isempty)
			return null;
		return this.cache.toString();
	}

	public String pop() throws IOException {
		String answer = peek();
		prefetchNextLine();
		return answer;
	}

	public boolean isEmpty() {
		return isempty;
	}

	private void prefetchNextLine() {
		try {
			this.cache = this.tbr.readLine();
			if (this.cache == null) {
				isempty = true;
			} else {
				isempty = false;
			}
		} catch (IOException e) {
			isempty = true;
			LOGGER.error("Some error happened while caching next line.", e);
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		this.tbr.close();
	}

	// This is not needed as the file created has been defined as a temp file.
	// But just to be on the safe side.
	public void delete() {
		this.filename.delete();
	}

	// public String push(){
	//
	// }

}
